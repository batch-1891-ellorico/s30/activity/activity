db.fruits.insertMany([
       {
           "name": "Banana",
           "supplier": "Farmer Fruits Inc.",
           "stocks": 30,
           "price": 20,
           "onSale": true
       },
       {
           "name": "Mango",
           "supplier": "Mango Magic Inc.",
           "stocks": 50,
           "price": 70,
           "onSale": true
       },
       {
           "name": "Dragon Fruit",
           "supplier": "Farmer Fruits Inc.",
           "stocks": 10,
           "price": 60,
           "onSale": true
       },
       {
           "name": "Grapes",
           "supplier": "Fruity Co.",
           "stocks": 30,
           "price": 100,
           "onSale": true
       },
       {
           "name": "Apple",
           "supplier": "Apple Valley",
           "stocks": 0,
           "price": 20,
           "onSale": false
       },
       {
           "name": "Papaya",
           "supplier": "Fruity Co.",
           "stocks": 15,
           "price": 60,
           "onSale": true
       }
]);

// #2 USE COUNT OPERATOR TO COUNT TOTAL # OF FRUITS ON SALE
db.fruits.aggregate([
	{
		$match: {"onSale": true}
	},
	{
		$group: {_id: "onSale", total: {$sum: 1}}
	}
])

// #3 USE COUNT OPERATOR TO COUNT TOTAL # OF FRUITS W/ STOCKS > 20
db.fruits.aggregate([
	{
		$match: {"stocks": {$gte: 20}}
	},
	{
		$group: {_id: "stocks", total: {$sum: 1}}
	}
])

// #4 USE AVERAGE OPERATOR TO GET AVERAGE PRICE OF FRUITS/SUPPLIER
db.fruits.aggregate([
	{
		$group: { _id: "$supplier", avgAmount: {$avg: "$price"}}
	}
])

// #5 USE MAX OPERATOR TO GET HIGHEST FRUIT PRICE PER SUPPLIER
db.fruits.aggregate([
	{
		$group: { _id: "$supplier", maxAmount: {$max: "$price"}}
	}
])

// #5 USE MAX OPERATOR TO GET HIGHEST FRUIT PRICE PER SUPPLIER
db.fruits.aggregate([
	{
		$group: { _id: "$supplier", minAmount: {$min: "$price"}}
	}
])